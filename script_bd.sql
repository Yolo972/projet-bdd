#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Salle
#------------------------------------------------------------

CREATE TABLE Salle(
        id         Int  Auto_increment  NOT NULL ,
        Code       Int  Auto_increment  NOT NULL ,
        Nom        Varchar ,
        NbPlace    Int ,
        Type_salle Char 
	,CONSTRAINT Salle_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Cours
#------------------------------------------------------------

CREATE TABLE Cours(
        Id         Int  Auto_increment NOT NULL ,
        Salle      Char ,
        Professeur Char  ,
        Groupes    Char 
	,CONSTRAINT Cours_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Responsable
#------------------------------------------------------------

CREATE TABLE Responsable(
        Id           Int  Auto_increment NOT NULL  ,
        Nom_respo    Char  ,
        Prenom_respo Char 
	,CONSTRAINT Responsable_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Coordonn�es
#------------------------------------------------------------

CREATE TABLE Coordonnees(
        Id            Int  Auto_increment  NOT NULL ,
        Num_nom_rue   Varchar  ,
        Code_postal   Int  ,
        Ville         Char  ,
        tel_domicile  Int ,
        tel_mobile    Int ,
        Mail          Varchar ,
        Id_Professeur Int
	,CONSTRAINT Coordonnees_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Identit�
#------------------------------------------------------------

CREATE TABLE Identite(
        Matricule               Int  Auto_increment  NOT NULL ,
        Nom                     Char  ,
        Prenom                  Char  ,
        Adresse                 Varchar  ,
        sexe                    Char  ,
        date_naissance          Date  ,
        Tel                     Varchar ,
        Ville                   Char ,
        Pays_de_naissance       Char ,
        Date_inscription        Date ,
        Etablissement_precedent Char ,
        Photo                   Char (5)  ,
        ID                      Int ,
        Id_Professeur           Int 
	,CONSTRAINT Identite_PK PRIMARY KEY (Matricule)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: El�ve
#------------------------------------------------------------

CREATE TABLE Eleve(
        ID        Int  Auto_increment  NOT NULL ,
        Matricule Int NOT NULL
	,CONSTRAINT Eleve_PK PRIMARY KEY (ID)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Professeur
#------------------------------------------------------------

CREATE TABLE Professeur(
        Id        Int  Auto_increment  NOT NULL ,
        Matricule Int NOT NULL
	,CONSTRAINT Professeur_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Groupe
#------------------------------------------------------------

CREATE TABLE Groupe(
        id            Int  Auto_increment  NOT NULL ,
        Eleves        Char ,
        Id_Professeur Int 
	,CONSTRAINT Groupe_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: r�serve
#------------------------------------------------------------

CREATE TABLE reserve(
        id            Int NOT NULL ,
        Id_Professeur Int NOT NULL
	,CONSTRAINT reserve_PK PRIMARY KEY (id,Id_Professeur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: _responsable de
#------------------------------------------------------------

CREATE TABLE _responsable_de(
        ID             Int NOT NULL ,
        Id_Responsable Int NOT NULL
	,CONSTRAINT _responsable_de_PK PRIMARY KEY (ID,Id_Responsable)
)ENGINE=InnoDB;




ALTER TABLE Coordonnees
	ADD CONSTRAINT Coordonnees_Professeur0_FK
	FOREIGN KEY (Id_Professeur)
	REFERENCES Professeur(Id);

ALTER TABLE Identite
	ADD CONSTRAINT Identite_Eleve0_FK
	FOREIGN KEY (ID)
	REFERENCES Eleve(ID);

ALTER TABLE Identite
	ADD CONSTRAINT Identite_Professeur1_FK
	FOREIGN KEY (Id_Professeur)
	REFERENCES Professeur(Id);

ALTER TABLE Identite 
	ADD CONSTRAINT Identite_Eleve0_AK 
	UNIQUE (ID);





	=======================================================================
	   D�sol�, il faut activer cette version pour voir la suite du script ! 
	=======================================================================
