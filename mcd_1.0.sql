#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Coordonn�es
#------------------------------------------------------------

CREATE TABLE Coordonnees(
        Id           Int  Auto_increment  NOT NULL ,
        Num_nom_rue  Varchar (50) NOT NULL ,
        Code_postal  Int NOT NULL ,
        Ville        Char (5) NOT NULL ,
        tel_domicile Int NOT NULL ,
        tel_mobile   Int NOT NULL ,
        Mail         Varchar NOT NULL
	,CONSTRAINT Coordonnees_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Salle
#------------------------------------------------------------

CREATE TABLE Salle(
        id         Int  Auto_increment  NOT NULL ,
        Code       Int  Auto_increment  NOT NULL ,
        Nom        Varchar (50) NOT NULL ,
        NbPlace    Int NOT NULL ,
        Type_salle Char (50) NOT NULL
	,CONSTRAINT Salle_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Cours
#------------------------------------------------------------

CREATE TABLE Cours(
        Id         Int  Auto_increment  NOT NULL ,
        Salle      Char (0) NOT NULL ,
        Professeur Char (50) NOT NULL ,
        Groupes    Char (50) NOT NULL
	,CONSTRAINT Cours_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Responsable
#------------------------------------------------------------

CREATE TABLE Responsable(
        Id           Int  Auto_increment  NOT NULL ,
        Nom_respo    Char (50) NOT NULL ,
        Prenom_respo Char (50) NOT NULL
	,CONSTRAINT Responsable_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Identit�
#------------------------------------------------------------

CREATE TABLE Identite(
        Matricule               Int  Auto_increment  NOT NULL ,
        Nom                     Char (50) NOT NULL ,
        Prenom                  Char (50) NOT NULL ,
        Adresse                 Varchar (250) ,
        sexe                    Char (5) ,
        date_naissance          Date ,
        Tel                     Varchar (10) ,
        Ville                   Char (5) NOT NULL ,
        Pays_de_naissance       Char (5) NOT NULL ,
        Date_inscription        Date NOT NULL ,
        Etablissement_precedent Char (5) NOT NULL ,
        Photo                   Char (5) NOT NULL
	,CONSTRAINT Identite_PK PRIMARY KEY (Matricule)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: El�ve
#------------------------------------------------------------

CREATE TABLE Eleve(
        ID Int  Auto_increment  NOT NULL
	,CONSTRAINT Eleve_PK PRIMARY KEY (ID)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Professeur
#------------------------------------------------------------

CREATE TABLE Professeur(
        Id Int  Auto_increment  NOT NULL
	,CONSTRAINT Professeur_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Groupe
#------------------------------------------------------------

CREATE TABLE Groupe(
        id     Int  Auto_increment  NOT NULL ,
        Eleves Char NOT NULL
	,CONSTRAINT Groupe_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: r�serve
#------------------------------------------------------------

CREATE TABLE reserve(
        id            Int NOT NULL ,
        Id_Professeur Int NOT NULL
	,CONSTRAINT reserve_PK PRIMARY KEY (id,Id_Professeur)

	,CONSTRAINT reserve_Salle_FK FOREIGN KEY (id) REFERENCES Salle(id)
	,CONSTRAINT reserve_Professeur0_FK FOREIGN KEY (Id_Professeur) REFERENCES Professeur(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: _responsable de
#------------------------------------------------------------

CREATE TABLE _responsable_de(
        ID             Int NOT NULL ,
        Id_Responsable Int NOT NULL
	,CONSTRAINT _responsable_de_PK PRIMARY KEY (ID,Id_Responsable)

	,CONSTRAINT _responsable_de_Eleve_FK FOREIGN KEY (ID) REFERENCES Eleve(ID)
	,CONSTRAINT _responsable_de_Responsable0_FK FOREIGN KEY (Id_Responsable) REFERENCES Responsable(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: _enseigne � 
#------------------------------------------------------------

CREATE TABLE _enseigne_a(
        ID            Int NOT NULL ,
        Id_Professeur Int NOT NULL
	,CONSTRAINT _enseigne_a_PK PRIMARY KEY (ID,Id_Professeur)

	,CONSTRAINT _enseigne_a_Eleve_FK FOREIGN KEY (ID) REFERENCES Eleve(ID)
	,CONSTRAINT _enseigne_a_Professeur0_FK FOREIGN KEY (Id_Professeur) REFERENCES Professeur(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: _assiste �
#------------------------------------------------------------

CREATE TABLE _assiste_a(
        id       Int NOT NULL ,
        Id_Cours Int NOT NULL




	=======================================================================
	   D�sol�, il faut activer cette version pour voir la suite du script ! 
	=======================================================================
